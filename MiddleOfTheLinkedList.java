class Solution {
    public ListNode middleNode(ListNode head) {
         if(head == null || head.next == null)
            return head;
        ListNode slow = head;
        ListNode fast = head;
        if(fast.next != null){
        while(fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        }
        return slow;
    }
}
        
    