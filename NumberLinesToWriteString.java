class Solution {
    public int[] numberOfLines(int[] widths, String s) {
          int[] result = new int[2];
        int line = 1;
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            int width = widths[s.charAt(i) - 'a'];
            if (count + width > 100) {
                line++;
                count = width;
            } else {
                count += width;
            }
        }
        result[0] = line;
        result[1] = count;
        return result;       
    }
}
        
    