class Solution {
    public boolean isPowerOfTwo(int n) {
        if(n<=0) return false;
	    int digit = (int)(Math.log(n)/Math.log(2));
	    return (1<<digit) == n; 
    }
}
    