class Solution {
    public String reverseWords(String s) {
         StringBuilder result = new StringBuilder();

        s = s.trim();
        String[] words = s.split(" ");
    
        for(int i=words.length-1; i>=0; i--){
            if(words[i].length() > 0)
            {
                result.append(words[i]);
                if(i>0) {
                    result.append(' ');
                }
            }
        }

        return result.toString();
    }
}
        
    